<?php

declare(strict_types=1);

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'params' => [
                    //LOCAL
                    //'url' => 'postgresql://localhost:5432/postgres',
                    'url' => 'postgresql://172.26.0.2:5432/postgres',
                    'driver' => 'pdo_pgsql',
                    'port' => '5432',
                    'host' => 'localhost',
                    'dbname' => 'postgres',
                    'user' => 'postgres',
                    'password' => 'root',
                    'charset' => 'UTF8'
                ],
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'App\Entity' => 'app_entity',
                ],
            ],
            'app_entity' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../../src/App/src/Entity']
            ],
        ],
    ],
];
