<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Suppliers\Entity\Supplier;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Zend\Diactoros\Response\JsonResponse;

class SuppliersUpdateHandler extends Validators implements RequestHandlerInterface
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $idSupplier = $request->getAttribute('id');
        $requestBody = $request->getParsedBody();

        if (empty($requestBody)) {
            $result['_error']['error'] = 'missing_request';
            $result['_error']['error_description'] = 'No request body sent.';
            return new JsonResponse($result, 400);
        }

        $entityRepository = $this->entityManager->getRepository(Supplier::class);
        $entity = $entityRepository->find($idSupplier);

        if (empty($entity)) {
            $result['_error']['error'] = 'not_found';
            $result['_error']['error_description'] = 'Record not found.';

            return new JsonResponse($result, 404);
        }

        try {
            $validators = $this->getValidator();
            if (!$validators['option']->filter($requestBody['option'])) {
                $result['_error']['error'] = 'filter_error';
                $result['_error']['error_description'] = 'Opcion dada es falso.';
                return new JsonResponse($result);
            }
            if (!$validators['name']->isValid($requestBody['fullname'])) {
                $result['_error']['error_description'] = $validators['name']->getMessages();

                return new JsonResponse($result);
            }
            $entity->setSupplier($requestBody);
            $this->entityManager->merge($entity);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            $result['_error']['error'] = 'not_updated';
            $result['_error']['error_description'] = $e->getMessage();

            return new JsonResponse($result, 400);
        }

        $result['_state']['state'] = 'success';
        $result['_state']['message'] = 'Supplier actualizado correctamente';

        return new JsonResponse($result);
    }
}
