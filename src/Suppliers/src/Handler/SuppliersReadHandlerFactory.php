<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Zend\Expressive\Helper\UrlHelper;

class SuppliersReadHandlerFactory
{
    public function __invoke(ContainerInterface $container) : SuppliersReadHandler
    {
        $entityManager = $container->get(EntityManager::class);
        
        $urlHelper = $container->get(UrlHelper::class);

        return new SuppliersReadHandler(
            $entityManager,
            $urlHelper
        );
    }
}
