<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;

class SuppliersDeleteHandlerFactory
{
    public function __invoke(ContainerInterface $container) : SuppliersDeleteHandler
    {
        return new SuppliersDeleteHandler(
            $container->get(EntityManager::class)
        );
    }
}
