<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Suppliers\Entity\Supplier;

use Doctrine\ORM\EntityManager;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Zend\Expressive\Helper\UrlHelper;
use Zend\Diactoros\Response\JsonResponse;

class SuppliersViewHandler implements RequestHandlerInterface
{
    protected $entityManager;
    protected $urlHelper;

    public function __construct(EntityManager $entityManager, UrlHelper $urlHelper)
    {
        $this->entityManager = $entityManager;
        $this->urlHelper = $urlHelper;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $idSupplier = $request->getAttribute('id');

        if (empty($idSupplier) || is_null($idSupplier)) {
            $result['_error']['error'] = 'param_not_found';
            $result['_error']['error_description'] = 'Parameter ID not found.';
            return new JsonResponse($result, 400);
        } else {
            $entityRepository = $this->entityManager->getRepository(Supplier::class);
            $entity = $entityRepository->find($idSupplier);

            if (empty($entity)) {
                $result['_error']['error'] = 'not_found';
                $result['_error']['error_description'] = 'Entity not found.';
                return new JsonResponse($result, 404);
            }

            $result['_state']['state'] = 'success';
            $result['_state']['message'] = 'Entity successfully obtained.';
            $result['_data'] = $entity->getSupplier();
            $result['_links']['self'] = $this->urlHelper->generate('suppliers.view', ['id' => $entity->getId()]);
            $result['_links']['delete'] = $this->urlHelper->generate('suppliers.delete', ['id' => $entity->getId()]);
            $result['_links']['update'] = $this->urlHelper->generate('suppliers.update', ['id' => $entity->getId()]);

            return new JsonResponse($result);
        }
    }
}
