<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Zend\Filter\Boolean;
use Zend\Validator\StringLength;

abstract class Validators
{
    private $validator;

    protected function getValidator() : array
    {
        $this->validator['name'] = new StringLength(['min' => 3, 'max' => 30, 'encoding' => 'UTF-8']);
        $this->validator['name']->setMessage("El contenido debe ser de una longitud menor a 30 y mayor a 3");
        $this->validator['option'] = new Boolean([
            'type'              =>  Boolean::TYPE_LOCALIZED,
            'translations'      =>  [
                'yes'   =>  true,
                'no'    =>  false,
                'si'    =>  true,
                'nein'  =>  false,
                'ja'    =>  true,
            ]
        ]);
        return $this->validator;
    }
}