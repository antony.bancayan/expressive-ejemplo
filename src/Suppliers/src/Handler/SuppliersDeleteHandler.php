<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Suppliers\Entity\Supplier;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Zend\Diactoros\Response\JsonResponse;

class SuppliersDeleteHandler implements RequestHandlerInterface
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $requestId = $request->getAttribute('id');

        if (!is_null($requestId) && !empty($requestId)) {
            $entityRepository = $this->entityManager->getRepository(Supplier::class);
            $entity = $entityRepository->find($requestId);

            if (!empty($entity)) {
                try {
                    $this->entityManager->remove($entity);
                    $this->entityManager->flush();
                } catch (ORMException $e) {
                    $result['_error']['error'] = 'not_removed';
                    $result['_error']['error_description'] = $e->getMessage();

                    return new JsonResponse($result, 400);
                }
                $result['_state']['state'] = 'success';
                $result['_state']['state_message'] = 'Supplier eliminado correctamente';

                return new JsonResponse($result);

            } else {
                $result['_error']['error'] = 'not_found';
                $result['_error']['error_description'] = 'Supplier not found.';
                return new JsonResponse($result, 404);
            }
        } else {
            $result['_error']['error'] = 'param_not_found';
            $result['_error']['error_description'] = 'Id Param not found.';
            return new JsonResponse($result, 400);
        }
    }
}
