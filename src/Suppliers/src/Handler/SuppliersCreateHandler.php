<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Suppliers\Entity\Supplier;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;


use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Helper\UrlHelper;

class SuppliersCreateHandler extends Validators implements RequestHandlerInterface
{
    protected $entityManager;
    protected $urlHelper;

    public function __construct(EntityManager $entityManager, UrlHelper $urlHelper)
    {
        $this->entityManager = $entityManager;
        $this->urlHelper = $urlHelper;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        // $requestBody = $request->getParsedBody()['Request']['Suppliers'];
        $requestBody = $request->getParsedBody();

        if (empty($requestBody)) {
            $result['_error']['error'] = 'missing_request';
            $result['_error']['error_description'] = 'No request body sent.';

            return new JsonResponse($result, 400);
        }

        $entity = new Supplier();

        try {
            $validators = $this->getValidator();
            if (!$validators['option']->filter($requestBody['option'])) {
                $result['_error']['error'] = 'filter_error';
                $result['_error']['error_description'] = 'Opcion dada es falsa';

                return new JsonResponse($result);
            }
            if ($validators['name']->isValid($requestBody['fullname'])) {
                $entity->setSupplier($requestBody);
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            } else {
                $result['_error']['error_descriptions'] = $validators['name']->getMessages();

                return new JsonResponse($result);
            }
        } catch (ORMException $e) {
            $result['_error']['error'] = 'not_created';
            $result['_error']['error_description'] = $e->getMessage();

            return new JsonResponse($result, 400);
        }

        $result['_state']['state'] = 'success';
        $result['_state']['state_message'] = 'Supplier created.';
        return new JsonResponse($result);
    }

}
