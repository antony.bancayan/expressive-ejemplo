<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class SuppliersUpdateHandlerFactory
{
    public function __invoke(ContainerInterface $container) : SuppliersUpdateHandler
    {
        return new SuppliersUpdateHandler(
            $container->get(EntityManager::class)
        );
    }
}
