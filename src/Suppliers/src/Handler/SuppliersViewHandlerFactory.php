<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\UrlHelper;

class SuppliersViewHandlerFactory
{
    public function __invoke(ContainerInterface $container) : SuppliersViewHandler
    {
        return new SuppliersViewHandler(
            $container->get(EntityManager::class),
            $container->get(UrlHelper::class)
        );
    }
}
