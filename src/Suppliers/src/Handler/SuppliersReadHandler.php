<?php

declare(strict_types=1);

namespace Suppliers\Handler;

use Suppliers\Entity\Supplier;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Zend\Diactoros\Response\JsonResponse;

use Zend\Expressive\Helper\UrlHelper;

class SuppliersReadHandler implements RequestHandlerInterface
{
    protected $entityManager;
    protected $urlHelper;

    public function __construct(EntityManager $entityManager, UrlHelper $urlHelper)
    {
        $this->entityManager = $entityManager;
        $this->urlHelper = $urlHelper;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $entityManager = $this->entityManager->getRepository('Suppliers\Entity\Supplier');

        $qb = $entityManager->createQueryBuilder('supplier');
        $qb->select('supplier')->orderBy('supplier.title', 'ASC');

        $array = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $result['_links']['self'] = $this->urlHelper->generate('suppliers.read');
        $result['_links']['create'] = $this->urlHelper->generate('suppliers.create');
        foreach ($array as $key => $value) {
            $array[$key]['_links']['self'] = $this->urlHelper->generate('suppliers.view', ['id' => $value['id']]);
            $array[$key]['_links']['update'] = $this->urlHelper->generate('suppliers.update', ['id' => $value['id']]);
            $array[$key]['_links']['delete'] = $this->urlHelper->generate('suppliers.delete', ['id' => $value['id']]);
            $array[$key]['_links']['view'] = $this->urlHelper->generate('suppliers.view', ['id' => $value['id']]);
        }

        $result['_embedded']['Suppliers'] = $array;
        return new JsonResponse($result);


    }
}
