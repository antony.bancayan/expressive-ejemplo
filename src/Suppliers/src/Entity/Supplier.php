<?php

namespace Suppliers\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="supplier")
 */

class Supplier
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="title")
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="label")
     */
    protected $label;

    /**
     * @ORM\Column(type="string", name="code")
     */
    protected $code;

    /**
     * @ORM\Column(type="string", name="fullname")
     */
    protected $fullName;

    /**
     * @param array $requestBody
     */
    public function setSupplier(array $requestBody): void
    {
        $this->setId($requestBody['id']);
        $this->setCode($requestBody['code']);
        $this->setFullName($requestBody['fullname']);
        $this->setTitle($requestBody['title']);
        $this->setLabel($requestBody['label']);
    }

    /**
     * @return array
     */
    public function getSupplier(): array
    {
        $data['id'] = $this->id;
        $data['title'] = $this->title;
        $data['label'] = $this->label;
        $data['fullname'] = $this->fullName;
        $data['code'] = $this->code;

        return $data;
    }

    /**
     * @param string $id
     */
    public function setId(string $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label) : void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getLabel() : string
    {
        return $this->label;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code) : void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName) : void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getFullName() : string
    {
        return $this->fullName;
    }
}