<?php

namespace Suppliers;

use Suppliers\Handler;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;

class RoutesDelegator
{
    /**
     * @param ContainerInterface $container
     * @param string $serviceName Name of the service being created.
     * @param callable $callback Creates and returns the service.
    */

    public function __invoke(ContainerInterface $container, $serviceName, callable $callback)
    {
        /** @var $app Application */
        $app = $callback();

        //Suppliers
        $app->post('/suppliers[/]', Handler\SuppliersCreateHandler::class, 'suppliers.create');
        $app->get('/suppliers/{id}[/]', Handler\SuppliersViewHandler::class, 'suppliers.view');
        $app->get('/suppliers[/]', Handler\SuppliersReadHandler::class, 'suppliers.read');
        $app->put('/suppliers/{id}[/]', Handler\SuppliersUpdateHandler::class, 'suppliers.update');
        $app->delete('/suppliers/{id}[/]', Handler\SuppliersDeleteHandler::class, 'suppliers.delete');

        return $app;
    }
}
