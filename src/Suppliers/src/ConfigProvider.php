<?php

declare(strict_types=1);

namespace Suppliers;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;

/**
 * The configuration provider for the Suppliers module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'doctrine'     => $this->getDoctrineEntities(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'delegators' => [
                \Zend\Expressive\Application::class => [
                    RoutesDelegator::class,
                ],
            ],
            'invokables' => [
            ],
            'factories'  => [
                Handler\SuppliersReadHandler::class     =>  Handler\SuppliersReadHandlerFactory::class,
                Handler\SuppliersCreateHandler::class   =>  Handler\SuppliersCreateHandlerFactory::class,
                Handler\SuppliersUpdateHandler::class   =>  Handler\SuppliersUpdateHandlerFactory::class,
                Handler\SuppliersDeleteHandler::class   =>  Handler\SuppliersDeleteHandlerFactory::class,
                Handler\SuppliersViewHandler::class     =>  Handler\SuppliersViewHandlerFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'suppliers'    => [__DIR__ . '/../templates/'],
            ],
        ];
    }

    public function getDoctrineEntities() : array
    {
        return [
            'driver' => [
                'orm_default' => [
                    'class' => MappingDriverChain::class,
                    'drivers' => [
                        'Suppliers\Entity' => 'supplier_entity',
                    ],
                ],
                'supplier_entity' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [__DIR__ . '/Entity'],
                ],
            ],
        ];
    }
}
