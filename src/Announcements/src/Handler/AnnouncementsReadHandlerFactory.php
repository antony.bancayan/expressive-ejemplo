<?php

declare(strict_types=1);

namespace Announcements\Handler;

use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;

class AnnouncementsReadHandlerFactory
{
    public function __invoke(ContainerInterface $container) : AnnouncementsReadHandler
    {
        $entityManager = $container->get(EntityManager::class);
        return new AnnouncementsReadHandler($entityManager);
    }
}
