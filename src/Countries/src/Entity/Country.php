<?php

namespace Countries\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="country")
 */

class Country
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id", nullable=false)
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="iso_code")
     */
    protected $isoCode;

    /**
     * @ORM\Column(type="string", name="name")
     */
    protected $name;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param string $isoCode
     */
    public function setIsoCode(string $isoCode) : void
    {
        $this->isoCode = $isoCode;
    }

    /**
     * @return string
     */
    public function getIsoCode() : string
    {
        return $this->isoCode;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}
