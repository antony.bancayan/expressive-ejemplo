<?php

declare(strict_types=1);

namespace Countries\Handler;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Zend\Diactoros\Response\JsonResponse;

class CountriesReadHandler implements RequestHandlerInterface
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $entityManager = $this->entityManager->getRepository('Countries\Entity\Country');
        $query = $entityManager->createQueryBuilder('u')->getQuery();

        return new JsonResponse($query->getResult(Query::HYDRATE_ARRAY));
    }
}
