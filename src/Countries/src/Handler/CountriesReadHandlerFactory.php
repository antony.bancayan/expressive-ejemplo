<?php

declare(strict_types=1);

namespace Countries\Handler;

use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;

class CountriesReadHandlerFactory
{
    public function __invoke(ContainerInterface $container) : CountriesReadHandler
    {
        $entityManager = $container->get(EntityManager::class);
        return new CountriesReadHandler($entityManager);
    }
}
