<?php

namespace Countries;

use Countries\Handler;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;

class RoutesDelegator
{
    /**
     * @param ContainerInterface $container
     * @param string $serviceName Name of the service being created.
     * @param callable $callback Creates and returns the service.
     */

    public function __invoke(ContainerInterface $container, $serviceName, callable $callback)
    {
        /** @var $app Application */
        $app = $callback();

        //Setup routes;
        $app->get('/countries[/]', Handler\CountriesReadHandler::class, 'countries.read');

        return $app;
    }
}
